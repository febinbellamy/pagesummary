<?php
namespace MediaWiki\Extension\PageSummary;

use MediaWiki\MediaWikiServices;
use MediaWiki\Page\ExistingPageRecord;
use MediaWiki\Rest\Handler;
use MediaWiki\Rest\HttpException;
use MediaWiki\Rest\Response;
use MediaWiki\Rest\SimpleHandler;
use Wikimedia\ParamValidator\ParamValidator;

/**
 * Handler for displaying or rendering a page summary:
 * - /v1/page/summary/{format}
 */
class RestHandler extends SimpleHandler {
	public function needsWriteAccess() {
		return false;
	}

	/**
	 * Handle request for page summary
	 * @param string $title
	 * @return Response
	 */
	public function run( $title ) {
		$page = $this->getPageByTitle( $title );
		$pageHtml = $this->getPageHtml( $page );
		$summaryOutput = new SummaryOutput( $title, $pageHtml );
		return $this->getResponseFactory()->createJson( $summaryOutput->generate() );
	}

	/**
	 * Get page by title
	 * @param string $title
	 * @return ExistingPageRecord
	 */
	private function getPageByTitle( $title ) {
		$services = MediaWikiServices::getInstance();
		$pageStore = $services->getPageStore();
		$page = $pageStore->getExistingPageByText( $title );
		if ( !$page ) {
			throw new HttpException( "Page not found", 404 );
		}

		return $page;
	}

	/**
	 * Get page html
	 * @param ExistingPageRecord $page
	 * @return string
	 */
	private function getPageHtml( $page ) {
		$services = MediaWikiServices::getInstance();
		$helperFactory = $services->getPageRestHelperFactory();
		$htmlHelper = $helperFactory->newHtmlOutputRendererHelper();
		$htmlHelper->init( $page, [], $this->getAuthority(), null );
		return $htmlHelper->getHtml()->getRawText();
	}

	/**
	 * Extract parameters from URL route
	 * @return array[]
	 */
	public function getParamSettings() {
		return [
			'title' => [
				Handler::PARAM_SOURCE => 'path',
				ParamValidator::PARAM_TYPE => 'string',
				ParamValidator::PARAM_REQUIRED => true,
			]
		];
	}
}
