<?php
namespace MediaWiki\Extension\PageSummary;

/**
 * Page summary output
 */
class SummaryOutput {
	/** @var string */
	public $title;

	/**
	 * @param string $title The title of the page to summarize
	 * @param string $pageHtml Parsoid access service
	 */
	public function __construct( $title, $pageHtml ) {
		$this->title = $title;
		$this->pageHtml = $pageHtml;
	}

	public function generate() {
		return [
			"description" => "Description for page $this->title"
		];
	}
}
