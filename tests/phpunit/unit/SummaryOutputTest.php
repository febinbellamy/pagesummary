<?php
use MediaWiki\Extension\PageSummary\SummaryOutput;
use PHPUnit\Framework\TestCase;

final class SummaryOutputTest extends TestCase {
	/**
	 * @covers MediaWiki\Extension\PageSummary\SummaryOutput::generate
	 */
	public function testSummaryEndpointStatusCode(): void {
		$title = "Main_Page";
		$html = "<html><body><div>Example</div></body></html>";
		$output = new SummaryOutput( $title, $html );
		$this->assertEquals( $output->title, $title );
		$this->assertEquals( [ "description" => "Description for page Main_Page" ], $output->generate() );
	}
}
